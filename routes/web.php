<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.jumbotron');
});

Route::get('/test-dompdf', function() {
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Auth::routes();
Route::get('/test-dompdf-2/{article}', 'pdfcontroller@test');

Route::get('/test-excel', 'ArticleController@export');


Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('article', 'ArticleController');
Route::resource('profile', 'profileController');

Route::get('/article', 'ArticleController@index' )->name('article.index');
Route::get('/article/create', 'ArticleController@create')->name('article.create');
Route::post('/article', 'ArticleController@store')->name('article.store');
Route::get('/article/{article}', 'ArticleController@show')->name('article.show');
Route::get('/article/{article}/edit', 'ArticleController@edit')->name('article.edit');
Route::put('/article/{article}', 'ArticleController@update')->name('article.update');
Route::delete('/article/{article}', 'ArticleController@destroy')->name('article.destroy');

Route::put('/comment/{article}', 'CommentController@store')->name('comment.store');