<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Article;

use App\Exports\ArticlesExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Comment;

use Auth;

class ArticleController extends Controller
{
    
    public function index()
    {
        $user = Auth::user();
        $article = $user->articles;
        return view('items.dataTabel', compact('article'));
    }

    public function create()
    {
        return view('items.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => 'required',
            "detail_article" => 'required',
            // "user_id" => 'required'
        ]); 
        
        $article = Article::create([
            "title" => $request["title"],
            "detail_article" => $request ["detail_article"],
            // "user_id" => Auth::id()
        ]);
            
        $user = Auth::user();
        $user->articles()->save($article);

        return view('items.SuccessEdit2');
    }

    public function show($id)
    {
        $article = Article::find($id);
        $comment = Comment::where('article_id', $id)->get();
        // $articles = compact('article');
        return view('items.show', compact('article', 'comment'));
    }

    public function edit($id)
    {
        $article = Article::find($id);
        return view('items.edit', compact('article'));
    }

    public function update(Request $request, $id)
    {

        $article = Article::where('id', $id)->update([
            "title" => $request["title"],
            "detail_article" => $request ["article"]
        ]);

        return view('items.SuccessEdit2');
    }

    public function destroy($id)
    {
        Article::destroy($id);
        return redirect('/article');
    }
    public function export() 
    {
        return Article::dowload(new ArticlesExport, 'Articles.xlsx');
    }
}
