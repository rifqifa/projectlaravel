<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Article;

class pdfcontroller extends Controller
{
    public function test($id){
        $data= Article::find($id);
        $pdf = PDF::loadView('pdf.test', compact('data'));
       return $pdf->download('test.pdf');
    }
} 
