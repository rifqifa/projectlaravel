<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class profileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all();
        return view('items.profil', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.createProfile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name" => 'required',
            "email" => 'required',
            "photo" => 'required',
            "bio" => 'required'

        ]);
        $profile = Profile::create([
            "name" => $request["name"],
            "email" => $request ["email"],
            "photo" => $request ["photo"],
            "bio" => $request ["bio"],
            "user_id"=> $request ["user_id"]  
        ]);
        return view('items.SuccessEdit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return view('items.profil', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $profile= Profile::find($id);
        return view('items.editProfile',compact('profile'));
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request )
    {
        $update = Profile:: where('id',$id)->update([
            "name"=> $request["name"],
            "email"=> $request["email"],
            "bio"=> $request["bio"]
        ]);
        // dd ($update);
        return view('items.SuccessEdit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
