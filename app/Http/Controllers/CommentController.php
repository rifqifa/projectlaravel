<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Article;
use DB;
use Auth;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        $article = Comment::create([
            "detail_comment" => $request ["comment"],
            "user_id" => Auth::id(),
            "article_id" => $id
        ]);

        return redirect()->route('article.show', $id);
    }

}
