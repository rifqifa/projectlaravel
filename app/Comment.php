<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "articlecomments";
    protected $guarded =  [];
    protected $fillable =  ["detail_comment","article_id","user_id"];

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function articles() {
        return $this->belongsTo('App\User', 'article_id');
    }
}
