<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";
    protected $guarded =  [];
    // protected $fillable =  ["title","detail_article","user_id"];

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments() {
        return $this->belongsToMany ('App\Comment', 'articlecomments', 'article_id', 'user_id');
    }

}