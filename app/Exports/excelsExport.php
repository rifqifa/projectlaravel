<?php

namespace App\Exports;

use App\excel;
use Maatwebsite\Excel\Concerns\FromCollection;

class excelsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return excel::all();
    }
}
