<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable =  ["name","email","photo","bio","user_id"];
    public $timestamps = true;
}
