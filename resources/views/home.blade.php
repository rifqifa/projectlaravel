@extends('layouts.app')

@section('content')
<div style="margin-top: 85px;"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-success" ><strong style="color:white;">Congratulation !!!</strong> </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br/>
                    Click icon user for Logout
                    <br/><br/>
                    <a href="/" class="btn btn-sm btn-outline-success">Go To Home</a>
                    <a href="{{ route('profile.create')}}" class="btn btn-sm btn-outline-success">Created Profile</a> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
