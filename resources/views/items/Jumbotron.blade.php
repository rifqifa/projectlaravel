@extends('welcome')

@section('content')
<header>
    <div class="jumbotron atas">
        <h1 class="display-4">Welcome to <span>TECHNO BLOG</span></h1>
        <p class="lead judul" style="color:white">This is a simple Blog, place of Information <a href="#" class="active">Teknologi</a></p>
        <div class="garis"></div>
        <p class="lead tombol">
            <a class="btn btn-lg" href="/article" role="button">Go To List</a>
        </p>
    </div>
    <div class="container topic">
        <div class="row">
            <div class="col-sm-4">
                <span class="text"> NEW ARTIKEL</span>
                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="text-center picture">
                            <img src="{{asset('/material/bigdata1.jpg')}}" class="rounded" alt="...">
                        </div>
                    </div>
                    <div class="col-sm-1 kanan">
                        <span style="font-size: 50%; letter-spacing: 2px;">DATABASE</span> 
                        <a href="#">Bigdata</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="text-center picture">
                            <img src="{{asset('/material/js.jpg')}}" class="rounded" alt="...">
                        </div>
                    </div>
                    <div class="col-sm-1 kanan">
                        <span style="font-size: 50%; letter-spacing: 2px; line-height: -40px;">WEB</span>
                       <a href="#">JavaScript</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="text-center picture">
                            <img src="{{asset('/material/ml.jpg')}}" class="rounded" alt="...">
                        </div>
                    </div>
                    <div class="col-sm-1 kanan">
                        <span style="font-size: 50%; letter-spacing: 2px;">MACHINE</span>
                       <a href="#">Machine Learning</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="text-center picture">
                            <img src="{{asset('/material/dv.jpg')}}" class="rounded" alt="...">
                        </div>
                    </div>
                    <div class="col-sm-1 kanan">
                        <span style="font-size: 50%; letter-spacing: 2px;">DATABASE</span>
                       <a href="#">Data Visualitation</a>
                    </div>
                </div>
                <div class="row see">
                    <div class="col-sm-4">
                        <a href="#" style="color: white; font-size: 80%; border-bottom: 2px solid rgb(255, 238, 0);">
                            SEE MORE
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 jumbo z-0">
                <div class="jumbotron" style="position: relative;">
                    <div class="container">
                      <h1 class="display-4 text1">Artificial Intelligence</h1>
                      <a href="#" class="text1 mt-2" style=" letter-spacing: 5px; margin-left:-20px; margin-top:-10px;font-size:60%; font-weight:bold; text-decoration:none; color:white;">
                        <img src="{{asset('/material/justified.png')}}" style="width: 15px; height:20px;" alt="">
                      READ</a>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</header>

    {{-- <section style="background-color: white;" class="mt-20">
        <h2>New Artikel</h2>
        <ul>
            <li>BIG DATA</li>
            <li>JAVASCRIPT</li>
            <li>MACHINE LEARNING</li>
            <li>DATA VISUALITATION</li>
    </ul>

</section> --}}

@endsection