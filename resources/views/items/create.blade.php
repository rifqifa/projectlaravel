@extends('welcome')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Article</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{route('article.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name ="title" value="{{old('title', '')}}" placeholder="title">
            </div>
            <div class="form-group">
                <label for="detail_article">detail_article</label>
                <input type="text" class="form-control" id="detail_article" name="detail_article" value="{{old('detail_article', '')}}" placeholder="detail_article">
            </div>
            <!-- <div class="form-group">
                <label for="user_id">user_id</label>
                <input type="text" class="form-control" id="user_id" name="user_id" value="{{old('user_id', '')}}" placeholder="user_id">
            </div> -->
        </div>
                <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>


@endsection