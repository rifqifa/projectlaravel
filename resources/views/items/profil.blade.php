@extends('welcome')

@section('content')
<div style="margin-top:100px"></div>

<div class="container">
<div class="card" style="width:50%;margin-left:25%">
  <center><img src="/material/photo.png" style="width:100px;height:100px;margin:10px" alt="photo" class="card-img-top"></center>
  <div class="card-body">
    <center><h4 class="card-title">PROFILE</h4></center>
    <ul class="list-group list-group-horizontal-xl">
      <li class="list-group-item" style="width:20%">Name
      <li class="list-group-item" style="width:80%">{{$profile->name}}</li>
    </ul>
    <ul class="list-group list-group-horizontal-xl">
      <li class="list-group-item" style="width:20%">Email
      <li class="list-group-item" style="width:80%">{{$profile->email}}</li>
    </ul>
    <ul class="list-group list-group-horizontal-xl">
      <li class="list-group-item" style="width:20%">Bio</li>
      <li class="list-group-item" style="width:80%">{{$profile->bio}}</li>
    </ul>
    <center>
    <div style="margin-top:20px">
      <a href="/profile/{{$profile->id}}/edit" class="btn btn-primary">Edit</a>
    </div>
    </center>
  </div>
</div>

@endsection