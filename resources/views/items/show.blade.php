@extends('welcome')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Article</h3>
    </div>
    <div class="card-header">
        <h3 class="card-title">Article</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <a href="/test-dompdf-2/{{$article->id}}" class="btn btn-sm btn-outline-success">Print PDF</a>
    <div class="card-body">
       
        <h1> {{$article->title}} </h1>
        <h3> Author : {{$article->author->name}} </h3>
        <h4> {{$article->detail_article}} </h4>
        
       
    </div>
    <a href="/article">Back</a>
            
                <!-- /.card-body -->
        <div class="card-footer">
            <h4> Comments </h4>
            @foreach ($comment as $comm)
            <div class="card card-primary mb-2">
                <div class="card-header">
                    <p>{{$comm->author->name}} comment: {{$comm->detail_comment}}</p>
                </div>    
            </div>    
            @endforeach

           
            <form role="form" action="{{route('comment.store',['article'=>$article->id])}}" method="post">
                @csrf
                @method('PUT')
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card-body">
                    <div class="form-group">
                        <label for="comment">Write a Comment</label>
                        <input type="text" class="form-control" id="comment" name ="comment" value="{{old('comment', '')}}" placeholder="comment">
                        <button type="submit" class="btn btn-primary mt-2">Submit</button>
                    </div>       
                </div>      
            </form>
           
        </div>
    </form>
</div>

@endsection