@extends('welcome')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Profile</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{route('profile.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">name</label>
                <input type="text" class="form-control" id="name" name ="name" value="{{old('name', '')}}" placeholder="name">
            </div>
            <div class="form-group">
                <label for="email">email mu adalah {{Auth::user()->email}}</label>
                <input type="text" class="form-control" id="email" name="email" value="{{old('email', '')}}" placeholder="email">
            </div>
            <div class="form-group">
                <label for="photo">photo</label>
                <input type="text" class="form-control" id="photo" name="photo" value="{{old('photo', '')}}" placeholder="photo">
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio', '')}}" placeholder="bio">
            </div>
            <div class="form-group">
                <label for="user_id">user_id mu adalah {{Auth::user()->id}}</label>
                <input type="text" class="form-control" id="user_id" name="user_id" value="{{old('user_id', '')}}" placeholder="user_id">
            </div>        
        </div>
                <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

@endsection