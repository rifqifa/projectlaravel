@extends('welcome')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Article</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{route('article.update',['article'=>$article->id])}}" method="post">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name ="title" value="{{old('title', $article->title)}}" placeholder="title">
            </div>
            <div class="form-group">
                <label for="article">Article</label>
                <input type="text" class="form-control" id="article" name="article" value="{{old('article', $article->detail_article)}}" placeholder="article">
            </div>        
        </div>
                <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

@endsection