@extends('welcome')

@section('content')
<div style="margin-top: 85px;"></div>
<div class="card">
      <div class="card-header">
        <h2 class="card-title"><strong>List Article</strong></h2>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      <a class="btn btn-primary mb-2" href="{{ route('article.create')}}">Create New Article</a>
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Title</th>
              <th>Detail Article</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach($article as $key => $data)
                <tr>
                    <td>{{$data->title}}</td>
                    <td>{{$data->detail_article}}</td>
                    <td>
                      <div style="display: flex;">
                      <a class="btn btn-sm btn-outline-success ml-2" href="{{ route('article.show', ['article'=> $data->id])}}">Show</a>
                      <a class="btn btn-sm btn-outline-primary ml-2" href="{{ route('article.edit', ['article'=> $data->id])}}">Edit</a>
                      <form action="{{ route('article.destroy', ['article'=> $data->id])}}" method="post">
                        @csrf 
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-sm btn-outline-danger ml-2">
                      </form>
                      <div>
                    </td>
                </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
@endsection

@push('scripts')
  <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('/dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('/dist/js/demo.js')}}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
    });
  </script>
@endpush