           
        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top shadow-lg">
            <a class="navbar-brand ml-3" href="/"><img style="width:100px;height:50px;" class="img-fluid"
                    src="/material/TechnoBlog.png" alt="TECHNO BLOG"></a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navigator">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navigator">
                <ul class="navbar-nav ml-auto">
                    <div class="btn-group mr-1">
                        <button type="button" class="btn btn-sm"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip"
                            data-placement="bottom" title="USER">
                            <img src="/material/usr1.png" style="width:20px;height:20px;" alt="User">
                        </button>
                        <div class="dropdown-menu dropdown-menu-lg-right">
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <li class="nav-item dropdown"≈>
                                
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endguest
                        </div>
                    </div>
                    <div class="btn-group mr-2">
                        <button type="button" class="btn btn-sm dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip"
                            data-placement="bottom" title="MENU">
                            <img src="/material/note.png" style="width:20px;height:20px;" alt="Menu">
                        </button>
                        <div class="dropdown-menu dropdown-menu-lg-right">
                            <form class="form-inline mt-3 mt-md-3">
                                <input class="form-control ml-2 mr-2" type="text" placeholder="Search"
                                    aria-label="Search">
                                <button class="btn btn-outline-success ml-2 mr-2 mt-sm-2"
                                    type="submit">search</span></button>
                            </form>
                            <div class="dropdown-divider"></div>
                            @if (Route::has('login'))
                                    @auth
                                    <li class="nav-item ml-2 mr-2">
                                        <a class="nav-link" href="/profile/{{Auth::user()->id}}"><img src="/material/profile.png"
                                            style="width:20px;height:20px;" alt="User"> Profil</a>
                                    </li>
                                    @else
                                    <li class="nav-item ml-2 mr-2">
                                        <a class="nav-link" href="#"><img src="/material/usr2.png"
                                            style="width:20px;height:20px;" alt="User"> Profil</a>
                                    </li>
                                    @endauth
                            @endif
                            <li class="nav-item ml-2 mr-2">
                                <a class="nav-link" href="#"><img src="/material/list.png"
                                        style="width:20px;height:20px;" alt="artikel"> List Artikel</a>
                            </li>
                        </div>
                    </div>
                </ul>
            </div>
    </nav>