<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com"> 
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/bootstrap-4.5.3-dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/css/style.css')}}">
        <link rel="icon" href="{{asset('/material/logo.png')}}">
        <!-- Styles -->
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @include('layouts.navbar')
            @yield('content')
            @include('layouts.footer')
        </div>
    </body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="{{asset('/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js')}}"></script>
@stack('scripts')
</html>
